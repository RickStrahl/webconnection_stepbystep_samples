************************************************************************
*PROCEDURE CustomerRestService
****************************
***  Function: Processes incoming Web Requests for CustomerRestService
***            requests. This function is called from the wwServer 
***            process.
***      Pass: loServer -   wwServer object reference
*************************************************************************
LPARAMETER loServer
LOCAL loProcess
PRIVATE Request, Response, Server, Session, Process
STORE NULL TO Request, Response, Server, Session, Process

#INCLUDE WCONNECT.H

loProcess = CREATEOBJECT("CustomerRestService", loServer)
loProcess.lShowRequestData = loServer.lShowRequestData

IF VARTYPE(loProcess)#"O"
   *** All we can do is return...
   RETURN .F.
ENDIF

*** Call the Process Method that handles the request
loProcess.Process()

*** Explicitly force process class to release
loProcess.Dispose()

RETURN

*************************************************************
DEFINE CLASS CustomerRestService AS WWC_RESTPROCESS
*************************************************************

*** Response class used - override as needed
cResponseClass = [WWC_PAGERESPONSE]

*** Scriptmode used for any non-method scriptmapped pages
*** 1 - MVC Page Modes (Templates/Scripts) 
*** 2 - Web Control Framework Pages
nPageScriptMode = 1

cCustomerCapitalizations = "firstName,lastName,billRate,careOf"

*********************************************************************
* Function CustomerRestService :: OnProcessInit
************************************
*** If you need to hook up generic functionality that occurs on
*** every hit against this process class , implement this method.
*********************************************************************
FUNCTION OnProcessInit

*!* LOCAL lcScriptName, llForceLogin
*!*	THIS.InitSession("MyApp")
*!*
*!*	lcScriptName = LOWER(JUSTFNAME(Request.GetPhysicalPath()))
*!*	llIgnoreLoginRequest = INLIST(lcScriptName,"default","login","logout")
*!*
*!*	IF !THIS.Authenticate("any","",llIgnoreLoginRequest) 
*!*	   IF !llIgnoreLoginRequest
*!*		  RETURN .F.
*!*	   ENDIF
*!*	ENDIF

*** Explicitly specify that pages should encode to UTF-8 
*** Assume all form and query request data is UTF-8
Response.Encoding = "UTF8"
Request.lUtf8Encoding = .T.


*** Add CORS header to allow x-site access from other domains/mobile devices
*** Uncomment for JSON Services and change * to allowed domain list
*!*  Response.AppendHeader("Access-Control-Allow-Origin","*")


*** Add CORS header to allow x-site access from other domains/mobile devices
Response.AppendHeader("Access-Control-Allow-Origin",Request.ServerVariables("HTTP_ORIGIN"))
Response.AppendHeader("Access-Control-Allow-Methods","POST, GET, DELETE, PUT, OPTIONS")
Response.AppendHeader("Access-Control-Allow-Headers","Content-Type, *")
*** Allow cookies and auth headers
Response.AppendHeader("Access-Control-Allow-Credentials","true")

lcVerb = Request.GetHttpVerb()
IF (lcVerb == "OPTIONS")
   *** Just exit with CORS headers set
   *** Required to make CORS work from Mobile devices
   RETURN .F.
ENDIF   

RETURN .T.
ENDFUNC


*********************************************************************
FUNCTION TestPage
***********************
LPARAMETERS lvParm
*** Any posted JSON string is automatically deserialized
*** into a FoxPro object or value


*** Simply create objects, collections, values and return them
*** they are automatically serialized to JSON
loObject = CREATEOBJECT("EMPTY")
ADDPROPERTY(loObject,"name","TestPage")
ADDPROPERTY(loObject,"description",;
            "This is a JSON API method that returns an object.")
ADDPROPERTY(loObject,"entered",DATETIME())

*** To get proper case you have to override property names
*** otherwise all properties are serialized as lower case in JSON
Serializer.PropertyNameOverrides = "Name,Description,Entered"


RETURN loObject

*** To return a cursor use this string result:
*!* RETURN "cursor:TCustomers"

*** Optionally you can also return non-JSON results with 
*!*	JsonService.IsRawResponse = .T.
*!*	Response.ExpandScript()
*!*	RETURN

ENDFUNC


************************************************************************
*  Customers
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Customers(loFilter)

lcWhere = ""
IF !ISNULLOREMPTY(loFilter)
   IF !ISNULLOREMPTY(loFilter.FirstName)
      lcWhere = "FirstName = loFilter.FirstName AND "
   ENDIF      
   IF !ISNULLOREMPTY(loFilter.LastName)
      lcWhere = lcWhere + "LastName = loFilter.LastName AND "
   ENDIF      
   IF !ISNULLOREMPTY(loFilter.Company)
      lcWhere = lcWhere + "Company = loFilter.Company AND "
   ENDIF      
ENDIF

IF !EMPTY(lcWhere)
	lcWhere = "WHERE " + RTRIM(lcWhere,"AND ","OR ")
ENDIF

SELECT id,FirstName,LastName,Company,Entered, Email, Phone, Address FROM Customers ;
      ORDER BY company ;
      &lcWhere ;
      INTO CURSOR TCustomers

Serializer.PropertyNameOverrides = "firstName,lastName"

RETURN "cursor:TCustomers"

*!*	loCustBus = CREATEOBJECT("cCustomer")

*!*	*** Business object loads TCustomers cursor
*!*	lnCount = loCustBus.GetCustomers() 
*!*	IF loCustBus.lError
*!*	   ERROR loCustBus.cErrorMsg  
*!*	ENDIF

*!*	loCustomers = CursorToCollection("TCustomers")

*!*	RETURN loCustomers
ENDFUNC
*   CustomerList


************************************************************************
*  Customer
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Customer(loParm)

lcVerb = Request.GetHttpVerb()

*** PUT and POST save, GET bypasses and shows customer
IF INLIST(lcVerb,"PUT","POST")
   RETURN THIS.SaveCustomer(loParm)
ENDIF   
IF lcVerb = "DELETE"
   RETURN THIS.DeleteCustomer(loParm)
ENDIF

lcId = Request.QueryString("id")  && string Pk
IF EMPTY(lcId)
   ERROR "Invalid customer id"
ENDIF

loCustBus = CREATEOBJECT("cCustomer")

IF !loCustBus.Load(lcId)
    ERROR loCustBus.cErrorMsg
ENDIF 

Serializer.PropertyNameOverrides = this.cCustomerCapitalizations

*** return oData member that contains customer object
RETURN loCustBus.oData
ENDFUNC
*   Customer


************************************************************************
*  SaveCustomer
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION SaveCustomer(loCust)

loCustBus = CREATEOBJECT("cCustomer")

IF !loCustBus.Load(loCust.id)
   *** creates empty oData with ID set
   loCustBus.New()
ENDIF

loData = loCustBus.oData

*!*	loData.Company = loCust.Company
*!*	loData.FirstName = loCust.FirstName
*!*	loData.LastName = loCust.LastName
*!*	loData.Address = loCust.Address
*!*	loData.Email = loCust.Email
*!*	loData.Phone = loCust.Phone

CopyObjectProperties(loCust,loCustBus.oData,1)

IF !loCustBus.Validate()
   *** will hold validation error
   ERROR loCustBus.cErrorMsg
ENDIF

IF !loCustBus.Save()
   ERROR loCustBus.cErrorMsg
ENDIF

RETURN loCustBus.oData   
ENDFUNC
*   SaveCustomer


************************************************************************
*  DeleteCustomer
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION DeleteCustomer()

lcId = Request.QueryString("id")

loCustBus = CREATEOBJECT("cCustomer")
IF !loCustBus.Delete(lcId)
   ERROR loCustBus.cErrorMsg
ENDIF

RETURN .T.
ENDFUNC
*   DeleteCustomer

*************************************************************
*** PUT YOUR OWN CUSTOM METHODS HERE                      
*** 
*** Any method added to this class becomes accessible
*** as an HTTP endpoint with MethodName.Extension where
*** .Extension is your scriptmap. If your scriptmap is .rs
*** and you have a function called Helloworld your
*** endpoint handler becomes HelloWorld.rs
*************************************************************


************************************************************************
*  ToDos
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION ToDos()

IF !FILE(".\todos.dbf")
	CREATE TABLE TODOS (title c(100), descript M, entered T,completed L)

	INSERT INTO TODOS VALUES ("Load up sailing gear","Load up the car, stock up on food.",DATETIME(),.f.)
	INSERT INTO TODOS VALUES ("Get on the road out East","Get in the car and drive until you find wind",DATETIME(),.f.)
	INSERT INTO TODOS VALUES ("Wait for wind","Arrive on the scene only to find no wind",DATETIME(),.f.)
	INSERT INTO TODOS VALUES ("Pray for wind","Still waiting!",DATETIME(),.F.)
	INSERT INTO TODOS VALUES ("Sail!","Score by hitting surprise frontal band and hit it big!",DATETIME(),.F.)
ENDIF

SELECT title, descript as Description, entered, completed FROM TODOS ;
	 ORDER BY entered ;
	 INTO CURSOR Tquery

RETURN "cursor_rawarray:TQuery"
ENDFUNC
*   ToDos

************************************************************************
*  ToDo
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION ToDo(loToDo)

*ERROR "We're not ready to accept your ToDo's just yet..."

#IF .F. 
LOCAL Request as wwRequest, Response as wwResponse, Server as wwServer, ;
      THIS as wwProcess, Process as wwProcess, Session as wwSession
#ENDIF


IF !USED("ToDos")
   USE ToDos IN 0
ENDIF
SELECT ToDos

lcVerb = Request.GetHttpverb()

IF lcVerb = "PUT" OR lcVerb = "POST"
	IF VARTYPE(loTodo) # "O"
	   ERROR "Invalid operation: No To Do Item passed."
	ENDIF


    LOCATE FOR TRIM(title) == loToDo.Title
    IF !FOUND()
		APPEND BLANK
	ENDIF
	GATHER NAME loTodo MEMO	
	
	*** Fix for differing field name
	REPLACE descript WITH loTodo.description
	
	*SCATTER NAME loToDo Memo
ENDIF

IF lcVerb = "DELETE"
   lcTitle = Request.QueryString("title")
   LOCATE FOR TRIM(title) == lcTitle
   IF !FOUND()
      ERROR "Invalid Todo - can't delete."
   ENDIF
   
   DELETE FOR TRIM(Title) == lcTitle
   RETURN null
ENDIF

RETURN loTodo
ENDFUNC
*   ToDo



ENDDEFINE