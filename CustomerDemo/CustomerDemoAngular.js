var app = angular.module('app', [
// 'ngRoute',
// 'ngSanitize',
// 'ngAnimate'
    ]);

// Add Routing
// app.config(['$routeProvider',
//      function($routeProvider) {
//           $routerProvider
//             .when("/albums", {
//                 templateUrl: "app/views/albums.html",
//             })
//             .otherwise({
//                 redirectTo: "/albums"
//             });
//      }
// ]);

app.controller('pageController', pageController);

pageController.$inject = ['$scope','$http','$timeout'];

function pageController($scope, $http, $timeout) {

    var vm = this;
    vm.customers = [];
    //    {
    //        firstName: "Rick",
    //        lastName: "Strahl",
    //        company: "West Wind Techologies",
    //        address: "32 Kaiea Place\nPaia, Hawaii",
    //        email: "rstrahl@west-wind.com",
    //        phone: "(808) 321-1231"
    //    },
    //    {
    //        firstName: "Markus",
    //        lastName: "Egger",
    //        company: "EPS Software",
    //        address: "312 Park Ave.\nWailea, Hawaii",
    //        email: "markus@eps-software.com",
    //        phone: "(808) 421-1231"
    //    }
    //];

    // methods
    vm.loadCustomers = loadCustomers;
    
    // Initialization
    initialize();    

    return;

    // Interface function implementations

    function initialize() {
        console.log("page controller started.");
        loadCustomers();
    }

    
    
    function loadCustomers() {
        return $http.get("customers.csvc")
            .success(function (customers) {
                    debugger;
                    vm.customers = customers;
            })
            .error(function() {
                    var error = ww.angular.parseHttpError(arguments);
                    toastr.error(error.message);
                });

    }
}
