LOCAL CRLF
CRLF = CHR(13) + CHR(10)
_out = []

 pcPageTitle = "Customer List - Customer Demo Sample" 

 IF (!wwScriptIsLayout)
    wwScriptIsLayout = .T.
    wwScriptContentPage = "c:\webconnectionprojects\webdemo\customerDemo\\customerlist.ctd"
    wwScript.RenderAspScript("~/views/_layoutpage.wcs")
    RETURN
ENDIF 
Response.Write([]+ CRLF +;
   []+ CRLF +;
   []+ CRLF +;
   [<div class="container">]+ CRLF +;
   [	 <a href="editcustomer.ctd"]+ CRLF +;
   [		  class="pull-right btn btn-sm btn-success">]+ CRLF +;
   [		  <i class="fa fa-pencil"></i> New]+ CRLF +;
   [	 </a>]+ CRLF +;
   []+ CRLF +;
   [	 <div class="page-header-text">] + CRLF )
Response.Write([		  <i class="fa fa-list-alt"></i> ]+ CRLF +;
   [		  Customer List <span class="b"></span>]+ CRLF +;
   [		  <span class="badge">])

Response.Write(TRANSFORM( EVALUATE([ pnCustomerCount ]) ))

Response.Write([</span>]+ CRLF +;
   [	 </div>	 ]+ CRLF +;
   []+ CRLF +;
   [	 <form action="" method="POST">]+ CRLF +;
   [		  <div class="input-group" style='margin: 20px 0;'>]+ CRLF +;
   [			 <span class="input-group-addon">]+ CRLF +;
   [				  <i class="fa fa-user"></i>				  ]+ CRLF +;
   [			 </span>]+ CRLF +;
   [			 <input type="text" name="Company" ]+ CRLF +;
   [					  class="form-control" ] + CRLF )
Response.Write([					  style="width: 200px;" ]+ CRLF +;
   [					  value="])

Response.Write(TRANSFORM( EVALUATE([ Request.FormOrValue("Company",pcCompany) ]) ))

Response.Write(["]+ CRLF +;
   [					  placeholder="Search Companies"]+ CRLF +;
   [					  />&nbsp;				]+ CRLF +;
   [				 <button type="submit" name="btnSearch" class="btn btn-default" type="button">]+ CRLF +;
   [				 Search...]+ CRLF +;
   [			 </button>	]+ CRLF +;
   [		  </div>]+ CRLF +;
   []+ CRLF +;
   [	 </form>]+ CRLF +;
   [	 <div class="container">] + CRLF )
Response.Write([])

 if pnCustomerCount < 1 
Response.Write([]+ CRLF +;
   [		  <div class="alert alert-warning">]+ CRLF +;
   [				<i class="fa fa-warning"></i>]+ CRLF +;
   [				There are no matching customers to display.		  ]+ CRLF +;
   [		  </div>]+ CRLF +;
   [])

 else 

Response.Write(TRANSFORM( EVALUATE([ pcGridHtml ]) ))


 endif 
Response.Write([]+ CRLF +;
   [	 </div>]+ CRLF +;
   [</div>				]+ CRLF +;
   []+ CRLF +;
   [<!-- remove sections if you're not using them -->]+ CRLF +;
   []+ CRLF +;
   []+ CRLF +;
   [])
