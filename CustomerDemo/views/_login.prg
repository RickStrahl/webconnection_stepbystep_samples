LOCAL CRLF
CRLF = CHR(13) + CHR(10)
_out = []

 pcPageTitle = "Application Sign in" 

 IF (!wwScriptIsLayout)
    wwScriptIsLayout = .T.
    wwScriptContentPage = "c:\webconnectionprojects\webdemo\customerDemo\views\_login.wcs"
    wwScript.RenderAspScript("~/views/_layoutpage.wcs")
    RETURN
ENDIF 


if vartype(pcUsername) !="C"
  pcErrorMessage =""
endif
if vartype(pcUsername) !="C"
  pcUserName =""
endif
if vartype(pcPassword) !="C"
  pcPassword =""
endif
if vartype(pcRedirectUrl) !="C"
  pcRedirectUrl =""
endif

Response.Write([]+ CRLF +;
   []+ CRLF +;
   [<div class="container">]+ CRLF +;
   [	 <div class="page-header-text">]+ CRLF +;
   [		  <i class="fa fa-unlock-alt"></i>]+ CRLF +;
   [		  Application Sign in]+ CRLF +;
   [	 </div>]+ CRLF +;
   []+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ HTmlErrorDisplay(pcErrorMessage,"warning") ]) ))

Response.Write([]+ CRLF +;
   []+ CRLF +;
   [	 <form action="" method="POST">]+ CRLF +;
   []+ CRLF +;
   [		  <div id="WebLogin" class="panel panel-primary" style="max-width: 550px; margin: 0 auto;">]+ CRLF +;
   [				<div class="panel-heading" style="font-weight: bold">]+ CRLF +;
   [					 Please sign in]+ CRLF +;
   [				</div>]+ CRLF +;
   [				<div class="panel-body">]+ CRLF +;
   [] + CRLF )
Response.Write([					 <div class="form-group">]+ CRLF +;
   [						  <div class="input-group ">]+ CRLF +;
   [								<div class="input-group-addon">]+ CRLF +;
   [									 <i class="fa fa-fw fa-envelope"></i>]+ CRLF +;
   [								</div>]+ CRLF +;
   [								<input type="text" name="WebLogin_txtUsername" id="WebLogin_txtUsername"]+ CRLF +;
   [										 class="form-control" placeholder="Enter your email address"]+ CRLF +;
   [										 value="])

Response.Write(TRANSFORM( EVALUATE([  pcUserName ]) ))

Response.Write(["]+ CRLF +;
   [										 autocapitalize="off"]+ CRLF +;
   [										 autocomplete="off"]+ CRLF +;
   [										 spellcheck="false"]+ CRLF +;
   [										 autocorrect="off" />]+ CRLF +;
   [						  </div>]+ CRLF +;
   [					 </div>]+ CRLF +;
   []+ CRLF +;
   [					 <div class="form-group">]+ CRLF +;
   [						  <div class="input-group ">] + CRLF )
Response.Write([								<div class="input-group-addon">]+ CRLF +;
   [									 <i class="fa fa-fw fa-unlock-alt"></i>]+ CRLF +;
   [								</div>]+ CRLF +;
   []+ CRLF +;
   [								<input type="password"]+ CRLF +;
   [										 name="WebLogin_txtPassword" id="WebLogin_txtPassword"]+ CRLF +;
   [										 class="form-control" id="form-group-input" placeholder="Enter your password">]+ CRLF +;
   [						  </div>]+ CRLF +;
   [					 </div>]+ CRLF +;
   [] + CRLF )
Response.Write([					 <button type="submit" name="WebLogin_btnLogin" ]+ CRLF +;
   [							  class="btn btn-primary btn-large"><i class="fa fa-unlock-alt"></i> Sign in</button>]+ CRLF +;
   []+ CRLF +;
   [])

 if Process.lIsAuthenticated 
Response.Write([]+ CRLF +;
   [					 	<a href="logout.])

Response.Write(TRANSFORM( EVALUATE([ JUSTEXT( Request.GetPhysicalPath() ) ]) ))

Response.Write(["]+ CRLF +;
   [						 		class="btn btn-default btn-sm pull-right">]+ CRLF +;
   [						 		<i class="fa fa-lock"></i> ]+ CRLF +;
   [						 		Sign out]+ CRLF +;
   [						  </a>]+ CRLF +;
   [])

 endif 
Response.Write([]+ CRLF +;
   [				</div>]+ CRLF +;
   [		  </div>]+ CRLF +;
   []+ CRLF +;
   [		  <input type="hidden" name="RedirectUrl" value="])

Response.Write(TRANSFORM( EVALUATE([ pcRedirectUrl ]) ))

Response.Write([" />]+ CRLF +;
   [	 </form>]+ CRLF +;
   [	 <script>]+ CRLF +;
   [	 	document.getElementById("WebLogin_txtUsername").focus();]+ CRLF +;
   [	 </script>]+ CRLF +;
   [</div>])
