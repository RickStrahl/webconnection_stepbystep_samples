LOCAL CRLF
CRLF = CHR(13) + CHR(10)
Response.Write([<!DOCTYPE html>]+ CRLF +;
   [<html xmlns="http://www.w3.org/1999/xhtml">]+ CRLF +;
   [<head>]+ CRLF +;
   [	 <title>])

Response.Write(TRANSFORM( EVALUATE([ IIF(vartype(pcPageTitle)="C",pcPageTitle,"") ]) ))

Response.Write([</title>]+ CRLF +;
   []+ CRLF +;
   [	 <meta charset="utf-8" />]+ CRLF +;
   [	 <meta http-equiv="X-UA-Compatible" content="IE=edge" />]+ CRLF +;
   [	 <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />]+ CRLF +;
   [	 <meta name="description" content="" />]+ CRLF +;
   []+ CRLF +;
   [	 <link rel="shortcut icon" href="~/favicon.ico" type="image/x-icon" />]+ CRLF +;
   [	 <meta name="apple-mobile-web-app-capable" content="yes" />]+ CRLF +;
   [	 <meta name="apple-mobile-web-app-status-bar-style" content="black" />] + CRLF )
Response.Write([	 <link rel="apple-touch-icon" href="~/touch-icon.png" />]+ CRLF +;
   []+ CRLF +;
   [	 <link rel="icon" href="~/touch-icon.png" />]+ CRLF +;
   [	 <meta name="msapplication-TileImage" content="~/touch-icon.png" />]+ CRLF +;
   []+ CRLF +;
   [	 <link href="~/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />	]+ CRLF +;
   [	 <link href="~/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />]+ CRLF +;
   [	 <link href="~/css/application.css" rel="stylesheet" />	 ]+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ wwScript.RenderSection("headers") ]) ))

Response.Write([]+ CRLF +;
   [</head>]+ CRLF +;
   [<body>]+ CRLF +;
   [	 <div class="banner">]+ CRLF +;
   [		  <!-- Slideout Menu Toggle - Hamburger menu -->]+ CRLF +;
   [		  <a class="slide-menu-toggle-open no-slide-menu"]+ CRLF +;
   [			  title="More..." href="~/">]+ CRLF +;
   [				<i class="fa fa-bars"></i>]+ CRLF +;
   [		  </a>]+ CRLF +;
   [] + CRLF )
Response.Write([		  <!-- Icon and Company Logo -->]+ CRLF +;
   [		  <div class="title-bar no-slide-menu">]+ CRLF +;
   [				<a href="~/customerlist.ctd">]+ CRLF +;
   [					 <img class="title-bar-icon" src="~/images/icon.png" />]+ CRLF +;
   [					 <div style="float: left; margin: 4px 5px; line-height: 1.0">]+ CRLF +;
   [						  <i style="color: #0092d0; font-size: 0.9em; font-weight: bold;">West Wind</i><br />]+ CRLF +;
   [						  <i style="color: whitesmoke; font-size: 1.65em; font-weight: bold;">Customer Demo</i>]+ CRLF +;
   [					 </div>]+ CRLF +;
   [				</a>]+ CRLF +;
   [		  </div>] + CRLF )
Response.Write([]+ CRLF +;
   [		  <!-- top right nav menu - .menu-hidable for options that hide on small sizes -->]+ CRLF +;
   [		  <nav class="banner-menu-top pull-right">]+ CRLF +;
   [])

 wwScript.RenderAspScript("~/Views/LoginMenu_Partial.wcs") 
Response.Write([]+ CRLF +;
   [				<a href="~/customerlist.ctd" class="menu-hidable">]+ CRLF +;
   [					 <i class="fa fa-list"></i>]+ CRLF +;
   [					 Customers]+ CRLF +;
   [				</a>]+ CRLF +;
   [				<a href="~/">]+ CRLF +;
   [					 <i class="fa fa-home"></i>]+ CRLF +;
   [					 Home]+ CRLF +;
   [				</a>]+ CRLF +;
   [] + CRLF )
Response.Write([		  </nav>]+ CRLF +;
   [	 </div>]+ CRLF +;
   []+ CRLF +;
   []+ CRLF +;
   [	 <div id="MainView">]+ CRLF +;
   []+ CRLF +;
   [])

 wwScript.RenderAspScript(wwScriptContentPage)
Response.Write([]+ CRLF +;
   []+ CRLF +;
   [	 </div> <!-- end #MainView -->]+ CRLF +;
   []+ CRLF +;
   []+ CRLF +;
   [	 <footer>]+ CRLF +;
   [		  <a href="http://www.west-wind.com/" class="pull-right">]+ CRLF +;
   [				<img src="~/images/WestwindText.png" />]+ CRLF +;
   [		  </a>]+ CRLF +;
   [		  <small>&copy; West Wind Inc., ])

Response.Write(TRANSFORM( EVALUATE([ Year(DateTime()) ]) ))

Response.Write([</small>]+ CRLF +;
   [	 </footer>]+ CRLF +;
   []+ CRLF +;
   []+ CRLF +;
   []+ CRLF +;
   [	 <!-- slide in menu - Remove if you don't use it -->]+ CRLF +;
   [	 <nav class="slide-menu">]+ CRLF +;
   [		  <div style="padding: 10px 10px 10px 3px;">]+ CRLF +;
   [				<a class="slide-menu-toggle-close">]+ CRLF +;
   [					 <i class="fa fa-bars"></i>] + CRLF )
Response.Write([				</a>]+ CRLF +;
   []+ CRLF +;
   [				<a class="disabled">]+ CRLF +;
   [					 <i class="fa fa-home"></i>]+ CRLF +;
   [					 Main Menu]+ CRLF +;
   [				</a>]+ CRLF +;
   []+ CRLF +;
   [				<a href="~/">]+ CRLF +;
   [					 <i class="fa fa-home"></i>]+ CRLF +;
   [					 Home] + CRLF )
Response.Write([				</a>]+ CRLF +;
   []+ CRLF +;
   [				<a href="#" class="indented">]+ CRLF +;
   [					 <i class="fa fa-newspaper-o"></i>]+ CRLF +;
   [					 Sub Link]+ CRLF +;
   [				</a>]+ CRLF +;
   [				<a href="#" class="indented">]+ CRLF +;
   [					 <i class="fa fa-newspaper-o"></i>]+ CRLF +;
   [					 Sub Link 2]+ CRLF +;
   [				</a>] + CRLF )
Response.Write([]+ CRLF +;
   [				<a href="#">]+ CRLF +;
   [					 <i class="fa fa-rss-square"></i>]+ CRLF +;
   [					 Login]+ CRLF +;
   [				</a>]+ CRLF +;
   [		  </div>]+ CRLF +;
   [	 </nav>]+ CRLF +;
   []+ CRLF +;
   [	 <script src="~/bower_components/jquery/dist/jquery.min.js"></script>]+ CRLF +;
   [	 <script src="~/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>] + CRLF )
Response.Write([	 <script>]+ CRLF +;
   [		  $(".slide-menu-toggle-open,.slide-menu-toggle-close," +]+ CRLF +;
   [			 ".slide-menu a, #SamplesLink")]+ CRLF +;
   [		  	.click(function () {]+ CRLF +;
   [		  		 $(".slide-menu").toggleClass("active");]+ CRLF +;
   [		  	});]+ CRLF +;
   [	 </script>]+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ wwScript.RenderSection("scripts") ]) ))

Response.Write([]+ CRLF +;
   [</body>]+ CRLF +;
   [</html>])
