(function () {
    var page = {
        activeCustomer: {},
        getCustomers: function getCustomerList() {
            // returns an asynchronous promise
            return $.getJSON("customers.csvc")
                .success(function(custList) {
                    page.customers = custList;

                    toastr.success("Customers downloaded.");

                    console.log(custList);

                    $("#CustomerList")
                        .listSetData(custList, { dataTextField: "company", dataValueField: "id" })
                        .triggerHandler("change"); //  force detail to update
                })
                .error(function(parm1, parm2, parm3) {
                    toastr.error("Customer List was not retrieved.");
                });
        },
        getCustomer: function getCustomer() {
            var id = $("#CustomerList").val();
            if (!id) {
                toastr.warning("Please select a customer.");
                return null;
            }

            return $.getJSON("customer.csvc?id=" + id)
                .success(function(cust) {                    
                    page.activeCustomer = cust;
                    toastr.success("Customer loaded:<br/> " + cust.company);

                    var template = $("#CustomerTemplate").html();
                    var html = parseTemplate(template, cust);

                    $("#CustomerContainer").html(html).show();

                })
                .error(function(err) {
                    toastr.error("Customer was not retrieved.");
                });
        },
        saveCustomer: function saveCustomer() {
            var cust = page.activeCustomer;
            cust.id = page.activeCustomer.id;
            if (!cust.id)
                id = 0; // new
            cust.lastName = $("#lastName").val();
            cust.firstName = $("#firstName").val();
            cust.company = $("#company").val();
            cust.address = $("#address").val();

            // from ww.jquery.js - posts object as json
           ajaxJson("customer.csvc", cust, { })
                .success(function () {                   
                    toastr.success("Customer data saved.");
                })
                .error(function (error) {
                    toastr.error("Failed to save customer data: " + error.message);
                });
        },
        initialize: function() {

            // Hook up event handlers
            $("#btnGetCustomerList").click(page.getCustomers);
            $("#CustomerList").change(page.getCustomer);

            // not rendered yet, so we need to use .on() and higher level DOM 
            // node plus a sub-selector to find at runtime.
            $(document).on("click","#btnSaveCustomer",page.saveCustomer);
        }
    };

    page.initialize();
})();