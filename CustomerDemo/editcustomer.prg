LOCAL CRLF
CRLF = CHR(13) + CHR(10)
_out = []

 pcPageTitle = "Edit Customer - Customer Demo Sample" 

 IF (!wwScriptIsLayout)
    wwScriptIsLayout = .T.
    wwScriptContentPage = "c:\webconnectionprojects\webdemo\customerDemo\editcustomer.ctd"
wwScriptSections.Add("scripts",[] + CRLF + ;
 [] +  CRLF +;
 [<script src="~/bower_components/jquery/dist/jquery.min.js"></script>] +  CRLF +;
 [] +  CRLF +;
 [<!-- datetimepicker related scripts and styles -->] +  CRLF +;
 [<link rel="stylesheet" ] +  CRLF +;
 [href="~/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.mi] +  CRLF +;
 [n.css" />] +  CRLF +;
 [] +  CRLF +;
 [<script src="~/bower_components/moment/min/moment.min.js"></script>] +  CRLF +;
 [<script ] +  CRLF +;
 [src="~/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.] +  CRLF +;
 [js"></script>] +  CRLF +;
 [] +  CRLF +;
 [<script>] +  CRLF +;
 [	 $("#Entered").not("[type='date'] + ']' + [").datetimepicker({] +  CRLF +;
 [	 	format: "MM/DD/YYYY",] +  CRLF +;
 [	 	keyBinds: { "delete": null }	// leave delete key] +  CRLF +;
 [	 });] +  CRLF +;
 [</script>] +  CRLF +;
 [])
    wwScript.RenderAspScript("~/views/_layoutpage.wcs")
    RETURN
ENDIF 
Response.Write([]+ CRLF +;
   []+ CRLF +;
   []+ CRLF +;
   [<div class="container">]+ CRLF +;
   [	 <a href="customerlist.ctd"  class="btn btn-link pull-right"]+ CRLF +;
   [		 title="Customer List">]+ CRLF +;
   [		  <i class="fa fa-list"></i> List]+ CRLF +;
   [	 </a>]+ CRLF +;
   [	 <div class="page-header-text">]+ CRLF +;
   [		  <i class="fa fa-edit"></i> ] + CRLF )
Response.Write([		  Edit Customer]+ CRLF +;
   [	 </div>]+ CRLF +;
   []+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ HtmlErrorDisplay(poError) ]) ))

Response.Write([]+ CRLF +;
   []+ CRLF +;
   [	 <form action="" method="POST"]+ CRLF +;
   [			 class="form-horizontal container"]+ CRLF +;
   [			 style="padding: 0 15px 30px;">]+ CRLF +;
   []+ CRLF +;
   [		  <div class="form-group">]+ CRLF +;
   [				<label class="col-sm-2">Company:</label>]+ CRLF +;
   [				<div class="col-sm-7">]+ CRLF +;
   [					 <input name="Company" id="Company"] + CRLF )
Response.Write([							  class="form-control" placeholder="Enter a company name"]+ CRLF +;
   [							  value="])

Response.Write(TRANSFORM( EVALUATE([ Request.FormOrValue(" Company", poCustomer.Company) ]) ))

Response.Write(["]+ CRLF +;
   [					 />]+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ HtmlBindingError("Company",poError.Errors) ]) ))

Response.Write([]+ CRLF +;
   [				</div>]+ CRLF +;
   [		  </div>]+ CRLF +;
   []+ CRLF +;
   [		  <div class="form-group form-horizontal">]+ CRLF +;
   [				<label class="col-sm-2">First Name:</label>]+ CRLF +;
   [				<div class="col-sm-7">]+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ HtmlTextBox("FirstName",poCustomer.FirstName,					  [class="form-control" placeholder="Enter the first name"] + ']' + [) ]) ))


Response.Write(TRANSFORM( EVALUATE([ HtmlBindingError("FirstName",poError.Errors) ]) ))

Response.Write([]+ CRLF +;
   [				</div>]+ CRLF +;
   [		  </div>]+ CRLF +;
   []+ CRLF +;
   [		  <div class="form-group form-horizontal">]+ CRLF +;
   [				<label class="col-sm-2">Last Name:</label>]+ CRLF +;
   [				<div class="col-sm-7">]+ CRLF +;
   [					 <input name="LastName" id="LastName"]+ CRLF +;
   [							  value="])

Response.Write(TRANSFORM( EVALUATE([ Request.FormOrValue(" LastName",poCustomer.LastName) ]) ))

Response.Write(["]+ CRLF +;
   [					 class="form-control" placeholder="Enter the last name" />]+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ HtmlBindingError("LastName",poError.Errors) ]) ))

Response.Write([]+ CRLF +;
   [				</div>]+ CRLF +;
   [		  </div>]+ CRLF +;
   []+ CRLF +;
   [		  <div class="form-group form-horizontal">]+ CRLF +;
   [				<label class="col-sm-2">Address:</label>]+ CRLF +;
   [				<div class="col-sm-7">]+ CRLF +;
   [					 <textarea name="Address"]+ CRLF +;
   [								  class="form-control"]+ CRLF +;
   [								  rows="4"] + CRLF )
Response.Write([								  placeholder="Enter the full address"]+ CRLF +;
   [					 >])

Response.Write(TRANSFORM( EVALUATE([ Request.FormOrValue("Address",poCustomer.Address) ]) ))

Response.Write([</textarea>]+ CRLF +;
   [				</div>]+ CRLF +;
   [		  </div>]+ CRLF +;
   []+ CRLF +;
   [		  <div class="form-group form-horizontal">]+ CRLF +;
   [				<label class="col-sm-2">Email:</label>]+ CRLF +;
   [				<div class="col-sm-7">]+ CRLF +;
   [					 <input name="Email" value="])

Response.Write(TRANSFORM( EVALUATE([ Request.FormOrValue('Email',poCustomer.Email) ]) ))

Response.Write(["]+ CRLF +;
   [							  class="form-control"]+ CRLF +;
   [							  placeholder="Enter the email address" />]+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ HtmlBindingError("Email",poError.Errors) ]) ))

Response.Write([]+ CRLF +;
   [				</div>]+ CRLF +;
   [		  </div>]+ CRLF +;
   []+ CRLF +;
   [		  <div class="form-group form-horizontal">]+ CRLF +;
   [				<label class="col-sm-2">Billing Rate:</label>]+ CRLF +;
   [				<div class="col-sm-7">]+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ HtmlTextBox("BillRate",poCustomer.BillRate,					  'class="form-control" type="number" step="0.5" pattern="[\d,-,.,\,] + ']' + ["') ]) ))


Response.Write(TRANSFORM( EVALUATE([ HtmlBindingError("BillRate",poError.Errors) ]) ))

Response.Write([]+ CRLF +;
   [				</div>]+ CRLF +;
   [		  </div>]+ CRLF +;
   []+ CRLF +;
   []+ CRLF +;
   [		  <div class="form-group form-horizontal">]+ CRLF +;
   [				<label for="Entered_Field" class="col-sm-2">Entered:</label>]+ CRLF +;
   [				<div class="col-sm-7">]+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ HtmlDateTextBox("Entered",poCustomer.Entered,"",0,[style="width: 250px;"] + ']' + [) ]) ))


Response.Write(TRANSFORM( EVALUATE([ HtmlBindingError("Entered",poError.Errors) ]) ))

Response.Write([]+ CRLF +;
   [				</div>]+ CRLF +;
   [		  </div>]+ CRLF +;
   []+ CRLF +;
   [		  <div class="form-group">]+ CRLF +;
   [				<div class="col-sm-2">&nbsp;</div>]+ CRLF +;
   [				<div class="col-sm-7">]+ CRLF +;
   [					 <div>]+ CRLF +;
   [])

Response.Write(TRANSFORM( EVALUATE([ HtmlCheckbox("InActive","Inactive Customer", poCustomer.InActive) ]) ))

Response.Write([]+ CRLF +;
   [					 </div>]+ CRLF +;
   [				</div>]+ CRLF +;
   [		  </div>]+ CRLF +;
   []+ CRLF +;
   [		  <hr />]+ CRLF +;
   []+ CRLF +;
   [		  <button type="submit" name="btnSubmit" class="btn btn-primary">]+ CRLF +;
   [				<i class="fa fa-check"></i> Save Customer]+ CRLF +;
   [		  </button>		  ] + CRLF )
Response.Write([	  <form>]+ CRLF +;
   [</div>				]+ CRLF +;
   []+ CRLF +;
   []+ CRLF +;
   [])
