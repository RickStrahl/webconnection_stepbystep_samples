(function() {
    var app = angular.module('app', [
// 'ngRoute',
// 'ngSanitize',
// 'ngAnimate'
    ]);

// Add Routing
// app.config(['$routeProvider',
//      function($routeProvider) {
//           $routerProvider
//             .when("/albums", {
//                 templateUrl: "app/views/albums.html",
//             })
//             .otherwise({
//                 redirectTo: "/albums"
//             });
//      }
// ]);

    app.controller('pageController', pageController);

    pageController.$inject = ['$scope', '$http', '$timeout', 'todoService'];

    function pageController($scope, $http, $timeout, todoService) {

        var vm = this;
        vm.message = null;
        vm.activeTodo = {
            title: null,
            description: null,
            completed: false
        };
        vm.todos = [];
        //    [{
        //        title: "SWFox Angular Presentation",
        //        description: "Try to show up on time this time",
        //        completed: false
        //    },
        //    {
        //        title: "Do FoxPro presentation",
        //        description: "Should go good, let's hope I don't fail...",
        //        completed: false
        //    },
        //    {
        //        title: "Do raffle at the end of day one",
        //        description: "Should go good, let's hope I don't fail...",
        //        completed: false
        //    },
        //    {
        //        title: "Arrive at conference",
        //        description: "Arrive in Phoenix and catch a cab to hotel",
        //        completed: true
        //    }
        //];

        vm.toggleCompleted = toggleCompleted;
        vm.addTodo = addTodo;
        vm.removeTodo = removeTodo;


        // Initialization
        initialize();

        return;

        // Interface function implementations
        function initialize() {
            console.log("page controller started.");
            loadTodos();
        }

        function toggleCompleted(todo) {
            todo.completed = !todo.completed;
        }

        function addTodo() {
            var todo = $.extend({}, vm.activeTodo);

            todoService.addTodo(todo)
                .success(function() {
                    // just rebind the list of todos
                    // which already has the merged todo in it
                    vm.todos = todoService.todos;
                })
                .error(function(error) {
                    vm.message = error.message;
                });

            // copy the tod
            //var td2 = $.extend({}, vm.activeTodo);

            //// now also post it to the server
            //postTodo(td2)
            //    .success(function() {

            //        // and add it to the model array
            //        vm.todos.splice(0, 0, td2);

            //        // clear out todo
            //        vm.activeTodo.title = null;
            //        vm.activeTodo.description = null;
            //    });
        }

        function removeTodo(todo, ev) {
            todoService.deleteTodo(todo)
                .success(function() {
                    vm.todos = todoService.todos;
                })
                .error(function(error) {
                    vm.message = "Couldn't delete Todo: " + error.message;
                });
        }

        function loadTodos() {
            todoService
                .getTodos()
                .success(function(todos) {
                    vm.todos = todos;
                })
                .error(function(error) {
                    vm.message = error.message;
                });

            //$http.get('todos.csvc')
            //    .success(function(todos) {
            //        vm.todos = todos;
            //    })
            //    .error(parseHttpError);
        }

        function postTodo(todo) {
            $http.post('todo.csvc', todo)
                .success(function(todo) {
                    showMessage("Todo Updated.");
                })
                .error(function(error) {
                    var error = ww.angular.parseHttpError(arguments);
                    vm.message = "An error occurred: " + error.message;
                });
        }

        function parseHttpError() {
            var error = ww.angular.parseHttpError(arguments);
            vm.message = "An error occurred: " + error.message;
        }

        function showMessage(msg) {
            vm.message = msg;
            $timeout(function() {
                vm.message = null;
            }, 5000);
        }
    }

    app.factory('todoService', todoService);

    todoService.$inject = ['$http', '$timeout'];

    function todoService($http, $timeout) {
        var service = {
            todos: [],

            getTodos: getTodos,
            addTodo: addTodo,
            deleteTodo: deleteTodo
        };


        return service;

        function getTodos() {
            return $http.get('todos.csvc')
                .success(function(todos) {
                    service.todos = todos;
                })
                .error(function() {
                    var error = parseHttpError(arguments);
                    return error;
                });
        }

        function addTodo(todo) {
            // copy the tod
            var td2 = $.extend({}, todo);

            $http.post('todo.csvc', todo)
                .success(function(todo) {
                    // and add it to the model array
                    service.todos.splice(0, 0, td2);
                })
                .error(function() {
                    return ww.angular.parseHttpError(arguments);
                });
        }

        function deleteTodo(todo) {
            $http.delete('todo.csvc?title=' + encodeURIComponent(todo.title))
                .success(function() {
                    // find the tdo and then remove it
                    var index = service.todos.indexOf(todo);
                    if (index > -1)
                        service.todos.splice(index, 1);
                })
                .error(function() {
                    return ww.angular.parseHttpError(arguments);
                });
        }
    }

})();