# Web Connection Step By Step Samples

This repository contains the fully completed samples for the [Step By Step guides in the West Wind Web Connection documentation](http://www.west-wind.com/webconnection/docs/?page=_0nb1al6fm.htm).

There are two samples here:

* **[Core Step By Step Guide](http://www.west-wind.com/webconnection/docs/?page=_0nb1al6fm.htm)**  
This is the basic walkthrough of using Web Connection to get started, create a new project and run a number of data operations using mostly code based logic.

* **[Core Step By Step with wwBusiness Object](http://www.west-wind.com/webconnection/docs/?page=_0i102wsai.htm)**  
This walkthrough expands on the Core walk through by demonstrating how to use wwBusiness objects and using MVC style Script templates to create a more flexible application architecture for building server based applications.